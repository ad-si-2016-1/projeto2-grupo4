/**
 * 
 */
package br.ufg.inf.ad.jogodavelhaws;


/**
 * @author weryquessantos
 *
 */
public class Out {
	
	/** Metodo para alertar ao jogador que posicao na matriz ja foi marcada */
	public String alertarPosiMarcada(){
		return "A posicao ja foi marcada.";
	}
	
	/** Metodo para limpar o console */
	public String limparConsole(){
		String limpa = null;

		for(int i = 0; i < 100; i++){
			limpa+="\r\n";
		}

		return limpa;
	}

	/** Metodo que exibe mensagem inicial do jogo */
	public String mostrarMsgBemVindo(){
		return "Bem-vindo ao Jogo da Velha!";
	}

	/** Metodo que exibe mensagem de estado de jogo empatado */
	public String mostrarMsgEmpate(){
		return "Empate!";
	}

	/** Metodo que informa o jogador que ele perdeu */
	public String mostrarMsgDerrota(){
		return "Tu perdestes!";
	}

	/** Metodo que informa o jogador que ele venceu */
	public String mostrarMsgVitoria(){
		return "Tu vencestes!";
	}

	/** Metodo que alertar o jogador sobre a ausencia de oponentes */
	public String mostrarMsgAguardeJogador(){
		return "Aguarde mais um jogador se conectar.";
	}

	/** Metodo que solicita resposta a pedido de jogo */
	public String mostrarMsgAceitarJogo(){
		return "Digite sim/nao para aceitar/recusar o pedido de jogo: ";
	}

	/** Metodo que informa o caractere que o cliente usara no jogo */
	public String mostrarMsgJogaCom(char caractereJogo){
		return "Tu jogas com "+ caractereJogo +".";
	}

	/** Metodo que informa aos jogadores que o jogo comecou */
	public String mostrarMsgJogoComecou() {
		return "O jogo comecou!";
	}
}

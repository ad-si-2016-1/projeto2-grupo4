/**
 * 
 */
package br.ufg.inf.ad.jogodavelhaws;

import java.util.List;
import java.util.ArrayList;

import javax.jws.WebService;

/**
 * @author weryquessantos
 * 
 */
@WebService
public class JogoDaVelha {
	/** Lista de objetos jogadores 
	 * @see Jogador */
	private List<Jogador> jogadores = new ArrayList<Jogador>(); 
	
	/** Instancia da classe matriz 
	 * @see Matriz */
	private static Matriz matriz = new Matriz();
	
	/** Instancia da classe Out
	 * @see Out */
	private Out visao = new Out();
	
	/** Metodo que retorna lista de jogadores 
	 * @return the jogadores */
	public List<Jogador> getListaClientes(){
		return jogadores;
	}
	
	/** Metodo para registrar jogadores
	 * @param cliente */
	public void registrarCliente(Jogador cliente){
		jogadores.add(cliente);
	}
	
	/** Metodo para marcar na matriz o caractere do jogador
	 * @param linha
	 * @param coluna 
	 * @param caractereJogo */
	public void marcarNaMatriz(int linha, int coluna, String caractereJogo){
		String[][] hash = matriz.getMatriz();
		
		if(checarPosMarcada(linha, coluna) == false){
			hash[linha][coluna] = caractereJogo;
			
			matriz.setMatriz(hash);
		}
		else{
			visao.alertarPosiMarcada();
		}		
	}
	
	/** Metodo para exibir a matriz 
	 * @return String */
	public String exibirMatriz(){
		String matrizExibida = null;
		String[][] hash = matriz.getMatriz();

		matrizExibida = "\r\n";
		for (int i = 0; i < 3; i++) {
			matrizExibida += " " + hash[i][0] + " | " + hash[i][1] + " | " + hash[i][2] + " ";

			if (i != 2) {
				matrizExibida += "\r\n---|---|---\r\n";
			}
		}
		matrizExibida += "\r\n";
		
		return matrizExibida;
	}
	
	/** Metodo para checar se algum jogador ja venceu 
	 * @return boolean */
	public String checarMatriz(){
		String[][] hash = matriz.getMatriz();
		String flag = " ";
		
		int i;
	    
		/** Verifica as 3 linhas a procura de sequencia igual */
	    for (i = 0; i < 3; i++) {
	        if ((hash[i][0].equals(hash[i][1])) && (hash[i][0].equals(hash[i][2]))) {
	            flag = hash[i][0];

				return flag;
	        }
	    }
	    
	    /** Verifica as 3 colunas a procura de sequencia igual */
	    for (i = 0; i < 3; i++) {
	        if ((hash[0][i].equals(hash[1][i])) && (hash[0][i].equals(hash[2][i]))) {
				flag = hash[0][i];

				return flag;
	        }
	    }
	    
	    /** Verifica a diagonal principal a procura de sequencia igual */
	    for (i = 0; i < 3; i++) {
	        if ((hash[0][0].equals(hash[1][1])) && (hash[1][1].equals(hash[2][2]))) {
				flag = hash[0][0];

				return flag;
	        }
	    }
	    
	    /** Verifica a diagonal secundaria a procura de sequencia igual */
	    for (i = 0; i < 3; i++) {
	        if ((hash[0][2].equals(hash[1][1])) && (hash[1][1].equals(hash[2][0]))) {
				flag = hash[0][2];

				return flag;
	        }
	    }
		
		return flag;
	}
	
	/** Metodo para checar se posicao na matriz ja foi marcada 
	 * @param linha
	 * @param coluna 
	 * @return boolean */
	public boolean checarPosMarcada(int linha, int coluna){
		String[][] hash = matriz.getMatriz();

		if(hash[linha][coluna].equals(" ")){
			return false;
		}
		
		return true;
	}

	/** Metodo para inicializar a matriz */
	public void inicializarMatriz() {
		matriz.inicializarMatriz();
	}

	/** Metodo que retorna objeto visao */
	public Out imprimir(){
		return visao;
	}
}

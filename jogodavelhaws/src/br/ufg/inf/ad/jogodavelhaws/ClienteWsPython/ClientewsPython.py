#!/usr/bin/env python
# -*- coding: utf-8 -*-
import xml
import suds
from suds.client import Client
import platform
import os
import sys
import time

#autor Thyeres N. T. Sousa
#Testado em ambiente Windows e Linux com Python 2.7e suds 0.4
'''Requisitos
Python 2.7
Biblioteca suds 0.4
obs: Requer python 2.7 instalado no sistema

Procedimento para instalação do biblioteca suds - Lightweight SOAP client

1 - Baixar o arquivo ez_setup.py no site
https://bootstrap.pypa.io/ez_setup.py

2 -Executar no digitar no console

ez_setup.py

3- Digitar no console

easy_install suds

no windows navegar pelo console ate a pasta Scrips dentro da pasta C:\Python27\Scripts

easy_install suds

Mais informações no site
https://pypi.python.org/pypi/setuptools
'''

url = "http://localhost:8080/webservices/jogodavelha?wsdl"      #URL so servidor WSDL
cliente = Client(url)                                           #variavel usada para invocar objetos e consumir o webservice
cliente.set_options(cache=None)                                 #limpar o cache do suds em caso de alterações no servidor wsdl

''' Variaveis globais para logica do cliente '''

totaljogadores = ''                                             #armazana o total retornado da lista de joagadores do servidor
caracterjogador = ''                                            #armazena caracter do jogador localmente
matrizantesadv = ''                                             #estado da  matriz do jogo antes da jogada do adversario
checamatriz = ''                                                #resultado da checagem da matriz em busca de vitoria

def RegistraJogador():

    ''' Metodo para Registrar jogador e seu respectivo caracter'''
    Limpa()
    print('Bem-vindo ao Jogo da Velha!\n')
    print('Informe o nome do jogador:')
    try:
        nome = str(sys.stdin.readline().encode())               #recebe nome do jogador informado
    except SyntaxError, NameError:
        print ("Insira um Nome valido")
    else:
        Jogador = cliente.factory.create('jogador')             #instância o objecto Jogador definidos no WSDL .
        Jogador.nomeJogador = nome                              #define o parametro nome do objeto
        totaljogadores = cliente.service.getListaClientes()     #consome o metodo getlistaCliente definido no WSDL
        if len(totaljogadores)==0:
           Jogador.caractereJogo = 'X'
        else:
           Jogador.caractereJogo = 'O'
        global caracterjogador
        caracterjogador = Jogador.caractereJogo                 #define o parametro caractere do objeto
        cliente.service.registrarCliente(Jogador)               #consome o metodo registraCliente definido no WSDL
        print("Ola %s" % Jogador.nomeJogador)

def Listarjogadores():

    '''Metodo para listar jogador registrados no servidor'''

    Limpa()
    print ("Jogadores Registrados no Servidor")
    b = '{}"[]'                                                 #armazena caracteres a serem removidos da lista de jogadores
    totaljogadores = cliente.service.getListaClientes()         #consome o metodo getlistaCliente definido no WSDL

    # algoritmo que formata a Lista consumida no WSDL
    for i in range(len(totaljogadores)):
        individuo = str(totaljogadores[i])
        i + 1
        for caractere in b:
            individuo = individuo.replace(caractere,"")
            individuo = individuo.replace("(jogador)","")
        print(individuo)

def VerificaVencedor():

    '''Metodo para verificar se existe um vencedor na Partida baseado nas regras de negocio do servidor'''

    global checamatriz
    checamatriz = cliente.service.checarMatriz()                #consome o metodo checarMatriz definido no WSDL que defini o estado do vencedor
    if (checamatriz==caracterjogador):
        print("Tu vencestes!")
        sys.exit()
    elif (checamatriz<>caracterjogador and checamatriz<>" "):
        Limpa()
        print(cliente.service.exibirMatriz())
        print("Tu perdestes!")
        sys.exit()
    elif (checamatriz==" " and cliente.service.exibirMatriz().count(" ")==18):
        Limpa()
        print(cliente.service.exibirMatriz())
        print("Empate!")
        sys.exit()

def MarcarJogada():

    ''' Metodo que marca uma jogada'''
    VerificaVencedor()
    Limpa()
    checaPosicao=False
    while True:
        matriz = cliente.service.exibirMatriz()                 #consome o metodo exibirMatriz definido no WSDL
        print(matriz)
        try:
            print("Tu jogas com %s." % caracterjogador)
            linha = input("Informe a posicao na linha: ")       #recebe jogada do teclado
            coluna = input("Informe a posicao na coluna: ")     #recebe jogada do teclado
        except SyntaxError, NameError:
            print("Posicao Invalida")
        else:
            checaPosicao = cliente.service.checarPosMarcada(linha,coluna) #consome o metodo checaPosicao definido no WSDL
            if (checaPosicao==False):
                marcarposicao = cliente.service.marcarNaMatriz(linha,coluna,caracterjogador) #consome o metodo marcarNaMatriz enviado jogada para o servidor
                break
            else:
                print("A posicao ja foi marcada.")
    global matrizantesadv
    matrizantesadv = cliente.service.exibirMatriz()             #aramazena matriz antes da jogada do adversario
    print(matrizantesadv)
    PullingJogada()

def PullingJogada():

    """ Metodo que verifica no servidor se o adversario efetuou jogada"""
    VerificaVencedor()
    while(True):
        matrizposjogadv = cliente.service.exibirMatriz()        #consome o metodo exibirMatriz definido no WSDL
        if matrizantesadv<>matrizposjogadv:
            break
        else:
            Limpa()
            print(matrizantesadv)
            print("Aguarde sua Vez")
            time.sleep(1)

def IniciarJogo():

    ''' Metodo que inicia a partida'''

    if cliente.service.exibirMatriz().find('null')<>-1:         #consome o metodo inicializarMatriz definido no WSDL
        cliente.service.inicializarMatriz()
    while True:
       verificajogadores = cliente.service.getListaClientes()   #consome o metodo getListaClientes definido no WSDL
       estadoincial = cliente.service.exibirMatriz()            #consome o metodo exibirMatriz definido no WSDL
       buscaX = estadoincial.find('X')                          #busca caractere 'x" na matriz para determinar se partida foi iniciada
       if (len(verificajogadores)==2):
          if(caracterjogador=='X'):
             MarcarJogada()
          elif (caracterjogador=='O' and buscaX==-1):
                Limpa()
                print ("Aguarde Jogador 'X' iniciar partida.\n")
                break
          elif (caracterjogador=='O' and buscaX<>-1):
                MarcarJogada()
       else:
           Limpa()
           print("Aguarde mais um jogador se conectar.\n")
           break
       time.sleep(1)
       Limpa()

def Limpa():

    '''Metodo que limpa a tela do terminal'''

    tiposo = platform.system()
    if tiposo == 'Windows':
        os.system("cls")
    if tiposo == 'Linux':
        os.system("clear")

def main():
    RegistraJogador()
    while True:
        print("---- Jogo da Velha ----")
        print(" 1 - Iniciar Jogo.")
        print(" 2 - Listar Jogadores.")
        #print(" 3 - LISTAR WSDL.")                     #VISUALIZAR OBJETOS DISPONIVEIS NO WEBSERVICE
        print(" 0 - Sair.          ")
        print("-----------------------")
        try:
            opcao = input("Digite o numero da opcao: ")
        except NameError, SyntaxError:
            print("Opcao invalida, verifique e tente novamente")
        else:
            if opcao == 1:
              IniciarJogo()
            elif opcao == 2:
              Listarjogadores()
            #elif opcao == 3:
            #  print(cliente)
            elif opcao == 0:
              break
            else:
                print("Opcao invalida, verifique e tente novamente")
                Limpa()

if __name__ == '__main__':
    main()
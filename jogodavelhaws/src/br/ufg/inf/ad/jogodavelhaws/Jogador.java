package br.ufg.inf.ad.jogodavelhaws;


/**
 * @author weryquessantos
 *
 */
public class Jogador {
	private String nomeJogador;
	private String caractereJogo;
	
	/** Metodo para retornar o nome do jogador
	 * @return the nomeJogador
	 */
	public String getNomeJogador() {
		return nomeJogador;
	}
	
	/** Metodo para retornar o caractere que o jogador joga
	 * @return the caractereJogo
	 */
	public String getCaractereJogo() {
		return caractereJogo;
	}
	
	/** Metodo para definir o nome do jogador
	 * @param nomeJogador the nomeJogador to set
	 */
	public void setNomeJogador(String nomeJogador) {
		this.nomeJogador = nomeJogador;
	}
	
	/** Metodo para definir o caractere que o jogador joga
	 * @param caractereJogo the caractereJogo to set
	 */
	public void setCaractereJogo(String caractereJogo) {
		this.caractereJogo = caractereJogo;
	}
	
}

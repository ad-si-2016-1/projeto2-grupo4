/**
 * 
 */
package br.ufg.inf.ad.jogodavelhaws;

import javax.xml.ws.Endpoint;

/**
 * @author weryquessantos
 *
 */
public class Servidor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		imprimirIniciado();
		Endpoint.publish("http://localhost:8080/webservices/jogodavelha", new JogoDaVelha());
	}

	private static void imprimirIniciado(){
		System.out.println("Servidor iniciado!");
	}
}

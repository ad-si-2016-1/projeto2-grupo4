/**
 * 
 */
package br.ufg.inf.ad.jogodavelhaws;

/**
 * @author weryquessantos
 *
 */
public class Matriz {
	private String[][] matriz = new String[3][3];

	/** Metodo para retornar a matriz
	 * @return the matriz
	 */
	public String[][] getMatriz() {
		return matriz;
	}

	/** Metodo para modificar a matriz completamente
	 * @param matriz the matriz to set
	 */
	public void setMatriz(String[][] matriz) {
		this.matriz = matriz;
	}
	
	/** Metodo para inicializar a matriz com caracteres vazios (" ")*/
	public void inicializarMatriz(){
		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				matriz[i][j] = " ";
			}
		}
	}
}

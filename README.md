# **Jogo da velha - Webservice**

## 1) Descrição

### 1.1) Resumo

O jogo passa-se num tabuleiro de 3×3 posições nas quais os jogadores
fazem suas marcas em uma das posições durante as rodadas.
O jogador que inicia a partida utiliza o símbolo “X”,
enquanto que o segundo jogador utiliza o símbolo “O”.

### 1.2) Quantidade de jogadores

O jogo da velha é jogado em turnos alternados entre dois jogadores.

### 1.3) Regras

Cada jogador é livre para colocar uma marca em qualquer posição no seu turno,
desde que a posição esteja vazia (sem marcas).
Ao colocar uma marca no tabuleiro, a jogada passa para o próximo jogador,
aonde o processo é repetido até que um dos jogadores vença,
ou até o tabuleiro ser completamente preenchido, situação na qual ocorre empate.

### 1.4) Como vencer

A vitória ocorre quando um jogador consegue colocar três símbolos em sequencia,
seja em linha, coluna ou na diagonal principal do tabuleiro.

O objetivo dos jogadores, então, é colocar três marcas numa das configurações válidas,
enquanto evitam que o seu oponente consiga fazer as três marcas antes dele.

Assim, os critérios de avaliação final de uma partida para o jogador são as seguintes:
– Vitória: o jogador consegue colocar as três marcas em linha.
– Empate: o tabuleiro é completamente preenchido e nenhum jogador, conseguem colocar as três marcas em linha.
– Derrota: o oponente consegue colocar as três marcas dele em linha

## 2)  Referências do jogo
<a href="http://regras.net/jogo-da-velha-e-como-ganhar-sempre/">Regras.net</a>
